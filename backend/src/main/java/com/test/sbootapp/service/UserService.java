package com.test.sbootapp.service;

import java.util.List;

import com.test.sbootapp.model.User;

/**
 * TODO: Document this interface.
 */
public interface UserService {

    User finduserById(long id);

    User findUserByLogin(String login);

    void createUser(User user);

    void updateUser(User user);

    void deleteUserById(long id);

    List<User> getAllUsers();

    boolean isUserExist(User user);

}
