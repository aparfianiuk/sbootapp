package com.test.sbootapp.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Service;

import com.test.sbootapp.model.User;

/**
 * TODO: Document this class.
 */

@Service("userService")
public class UserServiceImpl implements UserService {

    private static List<User> users = new ArrayList<>();

    static {
        users.add(new User(351, "Aliaksandr", "alex", 5231377));
        users.add(new User(659, "Igor", "igor", 6598534));
        users.add(new User(198, "Dmitry", "dima", 2658754));
    }

    private static List<User> userStorage() {
        return users;
    }

    @Override
    public User finduserById(final long id) {
        for (User user:userStorage()
             ) {
            if (user.getId() == id)
                return user;
        }
        return null;
    }

    @Override
    public User findUserByLogin(final String login) {
        for (User user:userStorage()
        ) {
            if (user.getLogin().equals(login))
                return user;
        }
        return null;
    }

    @Override
    public void createUser(final User user) {
        userStorage().add(user);
    }

    @Override
    public void updateUser(final User user) {
        int index = userStorage().indexOf(finduserById(user.getId()));
        userStorage().set(index, user);
    }

    @Override
    public void deleteUserById(final long id) {
        Iterator<User> iterator = userStorage().iterator();
        while (iterator.hasNext()){
            User user = iterator.next();
            if (user.getId() == id)
                iterator.remove();
        }
    }

    @Override
    public List<User> getAllUsers() {
        return userStorage();
    }

    @Override
    public boolean isUserExist(final User user) {
        return userStorage().contains(user);
    }

}
