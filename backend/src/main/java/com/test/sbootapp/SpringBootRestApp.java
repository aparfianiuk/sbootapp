package com.test.sbootapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * TODO: Document this class.
 */

@SpringBootApplication(scanBasePackageClasses = {com.test.sbootapp.RestApiController.class} )
public class SpringBootRestApp {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootRestApp.class, args);
    }

}
