package com.test.sbootapp.service;

import com.test.sbootapp.model.User;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


@Service("loginService")
public class LoginServiceImpl implements LoginService {


    RestTemplate restTemplate = new RestTemplate();

    String ENDPOINT = "http://backend:8855/api/login";

    @Override
    public User login(User someOne) throws RestClientException {
        return restTemplate.postForObject(ENDPOINT,someOne,User.class);
    }

}
