package com.test.sbootapp.service;

import com.test.sbootapp.model.User;
import org.springframework.web.client.RestClientException;

public interface LoginService {

    User login(User someOne) throws RestClientException;

}
