package com.test.sbootapp.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * TODO: Document this class.
 */

@SpringBootApplication(scanBasePackageClasses = {com.test.sbootapp.client.MainController.class, com.test.sbootapp.service.LoginServiceImpl.class})
public class SpringBootJspApp {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootJspApp.class, args);
    }

}
