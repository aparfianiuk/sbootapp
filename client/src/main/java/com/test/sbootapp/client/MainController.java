package com.test.sbootapp.client;

import com.test.sbootapp.model.User;
import com.test.sbootapp.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * TODO: Document this class.
 */

@Controller
@RequestMapping("/client")
public class MainController {

    @Autowired
    LoginService loginService;

    @ModelAttribute("user")
    User addUser() {
        return new User();
    }

    @RequestMapping(method = RequestMethod.GET)
    public String loginPage(){
        System.out.println("Login page");
       return "loginpage";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String tryLogin(@ModelAttribute("user") User someOne, Model model){
        User user = loginService.login(someOne);
        model.addAttribute("user", user);
        return "resultpage";
    }

}
