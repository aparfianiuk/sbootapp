<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Login to service</title>
    </head>
    <body>
        <h1>Login page</h1>
            <f:form method="POST" action="/client" accept-charset="UTF-8" modelAttribute="user">
            <table>
                <tr>
                    <td>
                        <i>Login:</i>
                    </td>
                    <td>
                        <f:input path="login" value="${user.login}"/>
                    </td>
                </tr>
                    <tr>
                    <td>
                        <i>Password:</i>
                    </td>
                    <td>
                        <f:input path="passwordHash" value="${user.passwordHash}" type="password"/>
                    </td>
                </tr>
                <tr>
                    <td><input type="submit" value="Login"/></td>
                </tr>
            </table>
        </f:form>
    </body>
</html>