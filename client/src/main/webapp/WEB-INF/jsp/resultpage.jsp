<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Welcome ${user.name}</title>
    </head>
    <body>
        <h1>Welcome to our service ${user.name}</h1>

        <h3>Your info:</h3>
        <p>ID: ${user.id}</p>
        <p>Name: ${user.name}</p>
        <p>Login: ${user.login}</p>
        <p>PWD: ${user.passwordHash}</p>

        <br/>
        <a href="/client">Back</a>
    </body>
</html>